package org.netbeans.apifest.boolcircuit;

import java.util.Optional;
import java.util.function.BinaryOperator;

/**
 * Created by santnil on 4/19/2017.
 */
public abstract class BiGate implements Gate {
    private Boolean inputOne;
    private Boolean inputTwo;
    private Boolean output;
    private Gate connectedTo;

    @Override
    public Optional<Boolean> apply() {
        if (inputOne != null && inputTwo != null) {
            output = getBinaryOperator().apply(inputOne, inputTwo);
            if (connectedTo != null) {
                connectedTo.setInput(output);
                return connectedTo.apply();
            }
            System.out.printf("Executed apply() with I1=%b, I2=%b and Yielding O=%b", inputOne, inputTwo, output);
        } else {
            System.out.printf("Unable to execute apply since not all inputs are set: inputOne=%b, inputTwo=%b",
                    inputOne, inputTwo);
        }
        return output==null ? Optional.empty() : Optional.of(output);
    }

    protected abstract BinaryOperator<Boolean> getBinaryOperator();

    @Override
    public void setInput(Boolean input) {
        if (inputOne == null) {
            inputOne = input;
        } else if (inputTwo==null) {
            inputTwo = input;
        } else {
            throw new RuntimeException("Input Conflict: trying to set input when both inputs are already set");
        }
    }

    @Override
    public Gate connectedTo() {
        return connectedTo;
    }

    public Boolean inputOne() {
        return inputOne;
    }

    public Boolean inputTwo() {
        return inputTwo;
    }

    public Boolean output() {
        return output;
    }

    public void setInputOne(Boolean inputOne) {
        this.inputOne = inputOne;
    }

    public void setInputTwo(Boolean inputOne) {
        this.inputOne = inputOne;
    }
}
