package org.netbeans.apifest.boolcircuit;

import java.util.Optional;
import java.util.function.BinaryOperator;

/**
 * Created by santnil on 4/19/2017.
 */
public class OrGate extends BiGate {
    BinaryOperator<Boolean> op;

    public OrGate() {
        this.op = new BinaryOperator<Boolean>() {
            @Override
            public Boolean apply(Boolean o1, Boolean o2) {
                return o1 || o2;
            }
        };
    }

    @Override
    protected BinaryOperator<Boolean> getBinaryOperator() {
        return this.op;
    }
}
