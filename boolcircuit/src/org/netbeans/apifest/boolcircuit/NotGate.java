package org.netbeans.apifest.boolcircuit;

import java.util.Optional;

/**
 * Created by santnil on 4/19/2017.
 */
public class NotGate implements Gate {
    private Gate connectedTo;
    private Boolean input;
    private Boolean output;

    @Override
    public Gate connectedTo() {
        return connectedTo;
    }

    @Override
    public Optional<Boolean> apply() {
        Optional<Boolean> result = Optional.empty();
        if (input != null) {
            output = !input;
            result = Optional.of(output);
        }
        return result;
    }

    @Override
    public void setInput(Boolean input) {
        this.input = input;
    }

    public void setConnectedTo(Gate connectedTo) {
        this.connectedTo = connectedTo;
    }
}
