package org.netbeans.apifest.boolcircuit;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.Optional;

/**
 * Created by santnil on 4/19/2017.
 */
public interface Gate {
    Gate connectedTo();
    Optional<Boolean> apply();

    void setInput(Boolean output);
}
